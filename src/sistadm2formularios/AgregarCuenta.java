/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistadm2formularios;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelo.Cuenta;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author pupi
 */
public class AgregarCuenta extends javax.swing.JInternalFrame {

    /**
     * Creates new form AgregarCuenta
     */
    public AgregarCuenta() {
        initComponents();
        agregarAutocompletadoACombos();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextNombre = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jComboMayorizaEn = new javax.swing.JComboBox();
        jButtonAgregarCuenta = new javax.swing.JButton();
        jCheckRecibeSaldo = new javax.swing.JCheckBox();

        setClosable(true);
        setTitle("Agregar Nueva Cuenta");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/sistadm2/imagenes/Plancuentas.png"))); // NOI18N
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Nueva Cuenta"));

        jLabel1.setText("Nombre:");

        jLabel4.setText("Mayoriza en:");

        jButtonAgregarCuenta.setText("Agregar Cuenta");
        jButtonAgregarCuenta.setActionCommand("Agregar");
        jButtonAgregarCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgregarCuentaActionPerformed(evt);
            }
        });

        jCheckRecibeSaldo.setText("Recibe Saldo");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonAgregarCuenta))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(jComboMayorizaEn, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(40, 40, 40)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jCheckRecibeSaldo)
                                    .addComponent(jTextNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 38, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckRecibeSaldo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboMayorizaEn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(16, 16, 16)
                .addComponent(jButtonAgregarCuenta)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        llenarCombos();
    }//GEN-LAST:event_formInternalFrameOpened

    private void jButtonAgregarCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgregarCuentaActionPerformed
        agregarCuenta((Cuenta)jComboMayorizaEn.getSelectedItem(), jCheckRecibeSaldo.isSelected(), jTextNombre.getText());
        
    }//GEN-LAST:event_jButtonAgregarCuentaActionPerformed

    private void agregarCuenta(Cuenta mayorizaEn, boolean recibeSaldo, String nombre) {
        if (jTextNombre.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, 
                    "Debe ingresar un nombre para la cuenta","Error",JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if (JOptionPane.showConfirmDialog(null, "¿Seguro que desea agregar la cuenta?","Se requiere confirmación",
                JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
            return;
        }
        
        Cuenta nuevaCuenta = new Cuenta();
        
        nuevaCuenta.setNombre(nombre);
        nuevaCuenta.setRecibeSaldo(recibeSaldo);
        int aux=-1;
        
        try {
            aux = mayorizaEn.obtenerNumeroHijos();
            System.out.println(aux);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                    "Error al leer las cuentas","Error",JOptionPane.ERROR_MESSAGE);
        }
        
        if (aux==-1) {
            return;
        }
        
        String codigo = mayorizaEn.getCodigo();
        codigo = codigo + "." + Integer.toString(aux + 1);        
        nuevaCuenta.setCodigo(codigo);
        
        String[] porciones = codigo.split("\\.");
        
        if (porciones[0].equals("1")) { //CUENTA DE ACTIVO (SUMA POR EL DEBE)            
            nuevaCuenta.setSumaDebe(true);
        }
        else if (porciones[0].equals("2")) { //PASIVO (NO SUMA POR EL DEBE)
            System.out.println("PASIVO");
            nuevaCuenta.setSumaDebe(false);
        }
        else if (porciones[0].equals("3")) { //PATRIMONIO (NO SUMA POR EL DEBE)
            System.out.println("PATRIMONIO");
            nuevaCuenta.setSumaDebe(false);
        }
        else if (porciones[0].equals("4")) { //INGRESOS (NO SUMA POR EL DEBE)
            System.out.println("INGRESOS");
            nuevaCuenta.setSumaDebe(false);
        }
        else if (porciones[0].equals("5")) { //INGRESOS (SUMA POR EL DEBE)
            System.out.println("EGRESOS");
            nuevaCuenta.setSumaDebe(true);
        }
        
        try {
            nuevaCuenta.insertar();
            JOptionPane.showMessageDialog(null, "Nueva cuenta agregada al plan de cuentas.");
            limpiarTexts();
        } catch (SQLException ex) {
            if (ex.getErrorCode() == 1062) {
                JOptionPane.showMessageDialog(null, 
                        "Ya existe una cuenta con el nombre especificado","Error",JOptionPane.ERROR_MESSAGE);
            }
        }
        
        
    }
    
    private void limpiarTexts() {
        jTextNombre.setText("");
        jCheckRecibeSaldo.setSelected(false);
        llenarCombos();
    }
    
    private void llenarCombos() {
        DefaultComboBoxModel cmb = (DefaultComboBoxModel) jComboMayorizaEn.getModel();
        cmb.removeAllElements();
        
        Cuenta c = new Cuenta();
        cuentas = c.obtenerTodos("");
        
        for (Cuenta cue: cuentas) {
            cmb.addElement(cue);
        }
        
        jComboMayorizaEn.setModel(cmb);
        
        
        
        
    }

    ArrayList<Cuenta> cuentas = new ArrayList<>();
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAgregarCuenta;
    private javax.swing.JCheckBox jCheckRecibeSaldo;
    private javax.swing.JComboBox jComboMayorizaEn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextNombre;
    // End of variables declaration//GEN-END:variables

    private void agregarAutocompletadoACombos() {
        AutoCompleteDecorator.decorate(this.jComboMayorizaEn);
        
        
    }

}
