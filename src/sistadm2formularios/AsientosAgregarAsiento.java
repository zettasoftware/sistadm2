/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistadm2formularios;

import com.sun.glass.events.KeyEvent;
import herramientas.FechaHora;
import herramientas.SQL;
import herramientas.Tablas;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.TabableView;
import modelo.Asiento;
import modelo.Cuenta;
import modelo.Linea;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author pupi
 */
public class AsientosAgregarAsiento extends javax.swing.JInternalFrame {

    /**
     * Creates new form AsientosAgregarAsiento
     */
    public AsientosAgregarAsiento() {
        initComponents();
        
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        jTableEntradasHaber.getColumnModel().getColumn(0).setCellRenderer(rightRenderer);
        
        
              
        agregarAutocompletadoACombos();
        //verificarDebeHaber();
        llenarCombos();
        
        
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jComboCuenta = new javax.swing.JComboBox();
        jButtonAgregar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jComboCodigo = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextMonto = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextDescripcion = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableEntradasDebe = new javax.swing.JTable();
        jLabelDebe = new javax.swing.JLabel();
        jLabelIgual = new javax.swing.JLabel();
        jLabelHaber = new javax.swing.JLabel();
        jButtonEliminarEntradaHaber = new javax.swing.JButton();
        jButtonMostrarDescripcionHaber = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTableEntradasHaber = new javax.swing.JTable();
        jButtonEliminarEntradaDebe = new javax.swing.JButton();
        jButtonMostrarDescripcionDebe = new javax.swing.JButton();
        jButtonGuardarAsiento = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("Agregar Asiento");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/sistadm2/imagenes/IconoLibroLittle.png"))); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Información del Asiento"));

        jComboCuenta.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboCuentaItemStateChanged(evt);
            }
        });
        jComboCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboCuentaActionPerformed(evt);
            }
        });
        jComboCuenta.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jComboCuentaPropertyChange(evt);
            }
        });

        jButtonAgregar.setText("Agregar");
        jButtonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgregarActionPerformed(evt);
            }
        });

        jLabel2.setText("Descripción:");

        jComboCodigo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboCodigoItemStateChanged(evt);
            }
        });
        jComboCodigo.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jComboCodigoPropertyChange(evt);
            }
        });

        jLabel1.setText("Codigo:");

        jLabel3.setText("Cuenta:");

        jLabel4.setText("Monto: $");

        jTextMonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextMontoKeyPressed(evt);
            }
        });

        jTextDescripcion.setColumns(20);
        jTextDescripcion.setRows(5);
        jTextDescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextDescripcionKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTextDescripcion);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addGap(21, 21, 21)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jComboCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 84, Short.MAX_VALUE))
                            .addComponent(jComboCuenta, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButtonAgregar)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButtonAgregar)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jTextMonto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(jLabel2))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jTableEntradasDebe.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jTableEntradasDebe.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cuenta", "DEBE", "HABER"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableEntradasDebe.setRowHeight(24);
        jTableEntradasDebe.getTableHeader().setReorderingAllowed(false);
        jTableEntradasDebe.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTableEntradasDebePropertyChange(evt);
            }
        });
        jScrollPane2.setViewportView(jTableEntradasDebe);
        if (jTableEntradasDebe.getColumnModel().getColumnCount() > 0) {
            jTableEntradasDebe.getColumnModel().getColumn(1).setMinWidth(50);
            jTableEntradasDebe.getColumnModel().getColumn(1).setMaxWidth(100);
            jTableEntradasDebe.getColumnModel().getColumn(2).setMinWidth(50);
            jTableEntradasDebe.getColumnModel().getColumn(2).setMaxWidth(100);
        }

        jLabelDebe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelDebe.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelDebe.setText("0");
        jLabelDebe.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelIgual.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelIgual.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelIgual.setText("=");
        jLabelIgual.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelHaber.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelHaber.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelHaber.setText("0");
        jLabelHaber.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jButtonEliminarEntradaHaber.setText("Borrar");
        jButtonEliminarEntradaHaber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEliminarEntradaHaberActionPerformed(evt);
            }
        });

        jButtonMostrarDescripcionHaber.setText("Mostrar Descripción");
        jButtonMostrarDescripcionHaber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonMostrarDescripcionHaberActionPerformed(evt);
            }
        });

        jTableEntradasHaber.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cuenta", "DEBE", "HABER"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableEntradasHaber.setRowHeight(24);
        jTableEntradasHaber.setTableHeader(null);
        jScrollPane4.setViewportView(jTableEntradasHaber);
        if (jTableEntradasHaber.getColumnModel().getColumnCount() > 0) {
            jTableEntradasHaber.getColumnModel().getColumn(1).setMinWidth(50);
            jTableEntradasHaber.getColumnModel().getColumn(1).setMaxWidth(100);
            jTableEntradasHaber.getColumnModel().getColumn(2).setMinWidth(50);
            jTableEntradasHaber.getColumnModel().getColumn(2).setMaxWidth(100);
        }

        jButtonEliminarEntradaDebe.setText("Borrar");
        jButtonEliminarEntradaDebe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEliminarEntradaDebeActionPerformed(evt);
            }
        });

        jButtonMostrarDescripcionDebe.setText("Mostrar Descripción");
        jButtonMostrarDescripcionDebe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonMostrarDescripcionDebeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 505, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jButtonEliminarEntradaHaber)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonMostrarDescripcionHaber, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelDebe)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelIgual)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelHaber))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jButtonEliminarEntradaDebe)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonMostrarDescripcionDebe, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonEliminarEntradaDebe)
                    .addComponent(jButtonMostrarDescripcionDebe))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonEliminarEntradaHaber)
                    .addComponent(jButtonMostrarDescripcionHaber)
                    .addComponent(jLabelDebe)
                    .addComponent(jLabelIgual)
                    .addComponent(jLabelHaber))
                .addContainerGap())
        );

        jButtonGuardarAsiento.setText("Guardar Asiento");
        jButtonGuardarAsiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGuardarAsientoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonGuardarAsiento)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonGuardarAsiento)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonGuardarAsientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGuardarAsientoActionPerformed
        verificarDebeHaber();
        
        if (jLabelIgual.getForeground().equals(Color.red)) {
            JOptionPane.showMessageDialog(null, "El asiento no está balanceado.");
            return;
        }
        if (jLabelIgual.getForeground().equals(Color.black)) {
            JOptionPane.showMessageDialog(null, "No hay lineas que guardar.");
            return;
        }
        
        asientoNuevo.setFecha(Calendar.getInstance().getTime());
        
        
        
        try {
            asientoNuevo.insertar();
        } catch (SQLException ex) {
            Logger.getLogger(AsientosAgregarAsiento.class.getName()).log(Level.SEVERE, null, ex);
        }
        int i;
        for (i=0;i<lineasDebe.size();i++) {
            try {
                lineasDebe.get(i).setId(i+1);
                lineasDebe.get(i).insertar();
            } catch (SQLException ex) {
                Logger.getLogger(AsientosAgregarAsiento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        int a = i;
        
        for (i=0;i<lineasHaber.size();i++) {
            try {
                lineasHaber.get(i).setId((i+a)+1);
                lineasHaber.get(i).insertar();
            } catch (SQLException ex) {
                Logger.getLogger(AsientosAgregarAsiento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        JOptionPane.showMessageDialog(null, "Asiento creado correctamente.");
        reestablecerFormulario();
        
        
            
        
    }//GEN-LAST:event_jButtonGuardarAsientoActionPerformed

    private void jTableEntradasDebePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTableEntradasDebePropertyChange
        
        if (evt.getPropertyName().equals("tableCellEditor")) {
            verificarDebeHaber();
        }
        
        //JOptionPane.showMessageDialog(null, evt.getPropertyName());
    }//GEN-LAST:event_jTableEntradasDebePropertyChange

    private void llenarCombos() {
        Cuenta c = new Cuenta();
        
        cuentas = c.obtenerTodos("");
        
        DefaultComboBoxModel dmCuenta = (DefaultComboBoxModel) jComboCuenta.getModel();
        DefaultComboBoxModel dmCodigo = (DefaultComboBoxModel) jComboCodigo.getModel();
        
        for (Cuenta cue : cuentas) {
            if (cue.isRecibeSaldo() == true) {
                dmCuenta.addElement(cue);
                dmCodigo.addElement(cue.getCodigo());
            }            
        }
        
        jComboCuenta.setModel(dmCuenta);
        jComboCodigo.setModel(dmCodigo);
        
        
    }
    
    private void jButtonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgregarActionPerformed
        double monto;
        
        try {
            monto = Double.parseDouble(jTextMonto.getText());
            if (monto == 0) throw new Exception("No puede ser 0");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error en el monto\n"+ex.getMessage());
            jTextMonto.setText("");
            jTextMonto.requestFocus();
            return;
        }
        
        
        Linea li = new Linea();
        li.setCuenta((Cuenta)jComboCuenta.getSelectedItem());
        li.setDescripcion(jTextDescripcion.getText());
        li.setMonto(monto);
        li.setAsiento(asientoNuevo);  
        
        if (li.getCuenta().isSumaDebe()) {
            if (li.getMonto() < 0) {
                lineasHaber.add(li);
            }
            else if (li.getMonto() > 0){
                lineasDebe.add(li);
            }
        }
        else {
            if (li.getMonto() < 0) {
                lineasDebe.add(li);
            }
            else if (li.getMonto() > 0) {
                lineasHaber.add(li);
            }
        }   
       
        
        
        actualizarTabla();        
        verificarDebeHaber();        
        limpiar();
        
    }//GEN-LAST:event_jButtonAgregarActionPerformed

    private void jButtonEliminarEntradaHaberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEliminarEntradaHaberActionPerformed
        
        if (jTableEntradasHaber.getSelectedRow() == -1) {
            return;
        }
        
        lineasHaber.remove(jTableEntradasHaber.getSelectedRow());
        
        actualizarTabla();
        verificarDebeHaber();
        
    }//GEN-LAST:event_jButtonEliminarEntradaHaberActionPerformed

    private void jComboCuentaPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jComboCuentaPropertyChange
/*        if (evt.getPropertyName().equals("COMBOBOX.CP_COMBOBOX")) {            
            jComboCodigo.setSelectedIndex(jComboCuenta.getSelectedIndex());            
        }*/
    }//GEN-LAST:event_jComboCuentaPropertyChange

    private void jComboCodigoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jComboCodigoPropertyChange
        /*if (evt.getPropertyName().equals("COMBOBOX.CP_COMBOBOX")) {            
            jComboCuenta.setSelectedIndex(jComboCodigo.getSelectedIndex());            
        }*/
    }//GEN-LAST:event_jComboCodigoPropertyChange

    private void jButtonMostrarDescripcionHaberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonMostrarDescripcionHaberActionPerformed
        JOptionPane.showMessageDialog(null, lineasHaber.get(jTableEntradasHaber.getSelectedRow()).getDescripcion());
    }//GEN-LAST:event_jButtonMostrarDescripcionHaberActionPerformed

    private void jComboCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboCuentaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboCuentaActionPerformed

    private void jButtonEliminarEntradaDebeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEliminarEntradaDebeActionPerformed
        if (jTableEntradasDebe.getSelectedRow() == -1) {
            return;
        }
        
        lineasDebe.remove(jTableEntradasDebe.getSelectedRow());
        
        actualizarTabla();
        verificarDebeHaber();
    }//GEN-LAST:event_jButtonEliminarEntradaDebeActionPerformed

    private void jButtonMostrarDescripcionDebeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonMostrarDescripcionDebeActionPerformed
        JOptionPane.showMessageDialog(null, lineasDebe.get(jTableEntradasDebe.getSelectedRow()).getDescripcion());
    }//GEN-LAST:event_jButtonMostrarDescripcionDebeActionPerformed

    private void jComboCodigoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboCodigoItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            if (jComboCuenta.getItemCount() > 0)
                jComboCuenta.setSelectedIndex(jComboCodigo.getSelectedIndex());
        }
    }//GEN-LAST:event_jComboCodigoItemStateChanged

    private void jComboCuentaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboCuentaItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            if (jComboCodigo.getItemCount() > 0)
                jComboCodigo.setSelectedIndex(jComboCuenta.getSelectedIndex());
        }
    }//GEN-LAST:event_jComboCuentaItemStateChanged

    private void jTextMontoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextMontoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButtonAgregarActionPerformed(new ActionEvent(this,0,""));
        }
    }//GEN-LAST:event_jTextMontoKeyPressed

    private void jTextDescripcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextDescripcionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButtonAgregarActionPerformed(new ActionEvent(this,0,""));
        }
    }//GEN-LAST:event_jTextDescripcionKeyPressed

    private void actualizarTabla() {
        Tablas.vaciarTabla(jTableEntradasDebe);
        Tablas.vaciarTabla(jTableEntradasHaber);
        
        DefaultTableModel tm = (DefaultTableModel)jTableEntradasDebe.getModel();
        DefaultTableModel tm2 = (DefaultTableModel)jTableEntradasHaber.getModel();
        
        
        
        for (Linea l : lineasDebe) {
            tm.addRow(new Object[]{l.getCuenta().getNombre(),Math.abs(l.getMonto()),0});
        }
        
        for (Linea l : lineasHaber) {
            tm2.addRow(new Object[]{l.getCuenta().getNombre(),0,Math.abs(l.getMonto())});
        }
        
        jTableEntradasDebe.setModel(tm);
        jTableEntradasHaber.setModel(tm2);
        
        jTableEntradasDebe.changeSelection(jTableEntradasDebe.getRowCount() - 1, 0, false, false);
        jTableEntradasHaber.changeSelection(jTableEntradasHaber.getRowCount() - 1, 0, false, false);
       
    }    
    
    
    private void limpiar() {
        jTextMonto.setText("");
        jTextDescripcion.setText("");
        jComboCodigo.setSelectedIndex(0);
        jComboCuenta.setSelectedIndex(0);
        jComboCodigo.requestFocus();
    }
    
    private void reestablecerFormulario() {        
        Tablas.vaciarTabla(jTableEntradasDebe);
        Tablas.vaciarTabla(jTableEntradasHaber);
        verificarDebeHaber();
        limpiar();
        lineasDebe.clear();
        lineasHaber.clear();
    }
    
    private void verificarDebeHaber() {
        int i;
        double auxDebe = 0;
        double auxHaber = 0;
        
        for (i=0;i<jTableEntradasDebe.getModel().getRowCount();i++) {
            
            auxDebe = auxDebe + Double.parseDouble(jTableEntradasDebe.getModel().getValueAt(i, 1).toString());
        }
        
        for (i=0;i<jTableEntradasHaber.getModel().getRowCount();i++) {
                       
            auxHaber = auxHaber + Double.parseDouble(jTableEntradasHaber.getModel().getValueAt(i, 2).toString());
        }
        
        if (auxHaber != auxDebe || auxHaber+auxDebe == 0) { //SI DIFIERE EL DEBE Y EL HABER
            jLabelDebe.setForeground(Color.red);
            jLabelHaber.setForeground(Color.red);
            jLabelIgual.setForeground(Color.red);
        }
        else {
            jLabelDebe.setForeground(Color.green);
            jLabelHaber.setForeground(Color.green);
            jLabelIgual.setForeground(Color.green);
        }
        
        jLabelDebe.setText(Double.toString(auxDebe));
        jLabelHaber.setText(Double.toString(auxHaber));
    }
    
    private void agregarAutocompletadoACombos() {
        AutoCompleteDecorator.decorate(this.jComboCuenta);
        AutoCompleteDecorator.decorate(this.jComboCodigo);
        
    }

    private ArrayList<Cuenta> cuentas = new ArrayList<>();
    private ArrayList<Linea> lineasDebe = new ArrayList<>();
    private ArrayList<Linea> lineasHaber = new ArrayList<>();
    
    private Asiento asientoNuevo = new Asiento();
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAgregar;
    private javax.swing.JButton jButtonEliminarEntradaDebe;
    private javax.swing.JButton jButtonEliminarEntradaHaber;
    private javax.swing.JButton jButtonGuardarAsiento;
    private javax.swing.JButton jButtonMostrarDescripcionDebe;
    private javax.swing.JButton jButtonMostrarDescripcionHaber;
    private javax.swing.JComboBox jComboCodigo;
    private javax.swing.JComboBox jComboCuenta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelDebe;
    private javax.swing.JLabel jLabelHaber;
    private javax.swing.JLabel jLabelIgual;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTableEntradasDebe;
    private javax.swing.JTable jTableEntradasHaber;
    private javax.swing.JTextArea jTextDescripcion;
    private javax.swing.JTextField jTextMonto;
    // End of variables declaration//GEN-END:variables
}
