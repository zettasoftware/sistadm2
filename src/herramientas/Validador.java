/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herramientas;

import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author pupi
 */
public class Validador {
    
    public static int booleanAInt(boolean b) {
        if (b) {
            return 1;
        }
        else {
            return 0;
        }
    }
    
        public static int getIntDeBoolean(boolean b) {
        if (b == true) {
            return 1;
        }
        else {
            return 0;
        }
    }
    
    private static boolean isIntegerParseInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException nfe) {}
        return false;
    }
    
    public static boolean isDoubleParseDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException nfe) {}
        return false;
    }
    
    
    
    public static boolean validarLargoTexto(String txt, int largo) {
        return txt.trim().length() <= largo && txt.trim().length() > 0;
       
    }
    
    public static boolean validarLargoTextoConVacio(String txt, int largo) {
        return txt.length() <= largo;
        
    }
    
    
    
    //-------------------------------------
    
    
    public static void validacionDeText(JTextField comp, int largo) {
        if (!Validador.validarLargoTexto(comp.getText(), largo)) {
            comp.setBackground(java.awt.Color.red);
        }
        else {
            comp.setBackground(java.awt.Color.WHITE);
        }
               
        
    }
    
    public static void validacionDeTextDouble(JTextField comp) {
        try {
            Double.parseDouble(comp.getText());
            comp.setBackground(Color.white);
        }    
        catch (NumberFormatException e){
            comp.setBackground(Color.red);
        }
            
    }
    
    
    public static void validacionDeTextArea(JTextArea comp, int largo) {
        if (!Validador.validarLargoTexto(comp.getText(), largo)) {
            comp.setBackground(java.awt.Color.red);
        }
        else {
            comp.setBackground(java.awt.Color.WHITE);
        }
    }
     
    
    
   
    
    public static void validacionDeInteger(JTextField comp) {
        if (!Validador.isIntegerParseInt(comp.getText())) {
            comp.setBackground(java.awt.Color.red);
        }
        else {
            comp.setBackground(java.awt.Color.WHITE);
        }
    }
    
    public static void validacionDeDouble(JTextField comp) {
        if (!Validador.isDoubleParseDouble(comp.getText())) {
            comp.setBackground(java.awt.Color.red);
        }
        else {
            comp.setBackground(java.awt.Color.WHITE);
        }
    }
    
    public static boolean isIntegerParseInteger(String str){
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException nfe) {}
        return false;
    }
    
}
