/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herramientas;

import com.mysql.jdbc.Connection;
import java.sql.*;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * @desc A singleton database access class for MySQL
 * @author Ramindu
 */

public final class SQL {
    public Connection conn;
    private Statement statement;
    public static SQL db;
    public int lastId;
    public static final int MYSQL_DUPLICATE_KEY = 1062;
    
    public static String host = "jdbc:mysql://localhost:3306/";
    public static String dbName = "sistadm2";
    public static String userName = "root";
    public static String password = "tompetty88";
    
    private SQL() {
        String url= this.host;
        String dbName = this.dbName;
        String driver = "com.mysql.jdbc.Driver";
        String userName = this.userName;
        String password = this.password;
        try {
            Class.forName(driver).newInstance();
            this.conn = (Connection)DriverManager.getConnection(url+dbName,userName,password);
        }
        catch (Exception sqle) {
            sqle.printStackTrace();
            JOptionPane.showMessageDialog(null, "No se pudo establecer una conexión a la Base de Datos.");
            JOptionPane.showMessageDialog(null, "El programa terminará ahora." + "\n"+ 
                     "Para solucionar el problema verifique los datos de conexion a la base de datos.");
            
            System.exit(0);
        }
    }
    /**
     *
     * @return MysqlConnect Database connection object
     */
    public static synchronized SQL getDbCon() {
        if ( db == null ) {
            db = new SQL();
        }
        return db;
 
    }
    /**
     *
     * @param query String The query to be executed
     * @return a ResultSet object containing the results or null if not available
     * @throws SQLException
     */
    public ResultSet query(String query) throws SQLException{
        statement = db.conn.createStatement();
        ResultSet res = statement.executeQuery(query);
        return res;
    }
    
    public void desconectar() {
        try {
            this.conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * @desc Method to insert data to a table
     * @param insertQuery String The Insert query
     * @return boolean
     * @throws SQLException
     */
    public int insert(String insertQuery) throws SQLException {
        
        statement = db.conn.prepareStatement("SQL_INSERT",Statement.RETURN_GENERATED_KEYS);
        int result = statement.executeUpdate(insertQuery,Statement.RETURN_GENERATED_KEYS);
        
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            setLastId((int) rs.getLong(1));
        }
        return result;
 
    }
    
    public int deleteUpdate(String insertQuery) throws SQLException {
        statement = db.conn.createStatement();
        int result = statement.executeUpdate(insertQuery);
        return result;
 
    }
    
    

    public int getLastId() {
        return lastId;
    }

    public void setLastId(int lastId) {
        this.lastId = lastId;
    }
 
}

