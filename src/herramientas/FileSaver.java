/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package herramientas;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pupi
 */
public class FileSaver {
    File file;
    FileWriter fw;
    OutputStreamWriter ow;
    
    
    public FileSaver (File file) {
        
        this.file = file;
        try {
            fw = new FileWriter(file);
        } catch (IOException ex) {
            Logger.getLogger(FileSaver.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public void guardar(String texto) throws IOException {
        
            
            fw.write(texto);
            fw.flush();
            fw.close();
       
        
    }
    
    
    
}
