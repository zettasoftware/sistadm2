/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herramientas;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author bibi
 */
public class FechaHora {
    
    public static String getFechaActual() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return (df.format(cal.getTime()));
    }
    
    public static String getHoraActual() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        return (df.format(cal.getTime()));
    }
    
    public static String fechaDMA(Date fecha) { //Devuelve la fecha en un String con formato DIA/MES/AÑO
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(fecha);
        
    }
    
    public static String fechaAMD(Date fecha) { //Devuelve la fecha en un String con formato AÑO/MES/DIA
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd"); 
        return df.format(fecha);        
    }
    
    public static String fechaHoraAMD(Date fecha) { //Devuelve la fecha y hora en String con formato yyyy/MM/dd hh:mm:ss
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss"); 
        return df.format(fecha);       
    }
    
    
}
