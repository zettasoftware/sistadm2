/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herramientas;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.text.StyleConstants;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.FontHighlighter;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.HighlighterFactory;

/**
 *
 * @author bibi
 */
public class Tablas {
    
    public static void reemplazarTrueFalse(JTable table, int col) {
        int i;
        
        for (i=0;i<table.getRowCount();i++) {
            if ((Boolean)table.getModel().getValueAt(i, col)) {
                table.getModel().setValueAt("SI", i, col);
            }
            else {
                table.getModel().setValueAt("NO", i, col);
            }
        }
        
    } 
    
    
    
    public static void resizeColumnWidth(JTable table) {
    final TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
        int width =120; // Min width
        for (int row = 0; row < table.getRowCount(); row++) {
            TableCellRenderer renderer = table.getCellRenderer(row, column);
            Component comp = table.prepareRenderer(renderer, row, column);
            width = Math.max(comp.getPreferredSize().width, width);
        }
        columnModel.getColumn(column).setPreferredWidth(width);
    }
}
    
    public static void cargarTabla (JTable tabla, ResultSet rs, String[] columnas) throws SQLException {
        // TableModel definition
        //DefaultTableModel aModel = (DefaultTableModel) tabla.getModel();
        //ESTO EVITA QUE LAS CELDAS DE LAS TABLAS PUEDAN SER EDITADAS
        DefaultTableModel aModel = new DefaultTableModel() {
        @Override
            public boolean isCellEditable(int row, int column) {
                //todas las celdas seran NO EDITABLES
                return false;
            }
        };
        //ACA TERMINA LO ANTERIOR.


        aModel.setColumnIdentifiers(columnas);
        

        // Loop through the ResultSet and transfer in the Model
        java.sql.ResultSetMetaData rsmd = rs.getMetaData();
        int colNo = rsmd.getColumnCount();
        
        while(rs.next()){
            Object[] objects = new Object[colNo];
            // tanks to umit ozkan for the bug fix!
            for(int i=0;i<colNo;i++){
                objects[i]=rs.getObject(i+1);
            }
            aModel.addRow(objects);
        }
        tabla.setModel(aModel);
        tabla.setAutoCreateRowSorter(true);
    
       
    
    }
    
    public static void vaciarTabla(JTable tabla) {
        DefaultTableModel tm = (DefaultTableModel)tabla.getModel();
        for( int i = tm.getRowCount() - 1; i >= 0; i-- ) {
            tm.removeRow(i);
        }
        tabla.setModel(tm);
    }
    
    public static void alinearDerecha(JTable tabla){
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        
    
        int i;
        for (i=0;i<tabla.getColumnModel().getColumnCount();i++) {
            tabla.getColumnModel().getColumn(i).setCellRenderer(rightRenderer);
        }
        
        
    
    }
    
    public static void alinearDerecha(JXTable tabla){
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        
    
        int i;
        for (i=0;i<tabla.getColumnModel().getColumnCount();i++) {
            tabla.getColumnModel().getColumn(i).setCellRenderer(rightRenderer);
        }       
    
    }
    
    public static void alinearCentro(JXTable tabla){
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        
    
        int i;
        for (i=0;i<tabla.getColumnModel().getColumnCount();i++) {
            tabla.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
        }       
    
    }
    
    public static void centrarUnaColumna(JXTable tabla, int columna){
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        
    
        Highlighter hl = new FontHighlighter(new Font(Font.SERIF,Font.BOLD,14));
        tabla.getColumnExt(columna).setHighlighters(hl);
       
        tabla.getColumnModel().getColumn(columna).setCellRenderer(centerRenderer);
             
    
    }
    
    public static void alinearEncabezadoAlCentro(JTable tabla) {
        DefaultTableCellRenderer tcr = (DefaultTableCellRenderer)tabla.getTableHeader().getDefaultRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tabla.getTableHeader().setDefaultRenderer(tcr);
    }
    
    
    
    public static void prepararJXTable(JXTable tabla) {
        tabla.packAll();
        tabla.addHighlighter(HighlighterFactory.createSimpleStriping());
    }
    
    
}
