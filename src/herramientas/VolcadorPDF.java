/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herramientas;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import modelo.Cuenta;

/**
 *
 * @author pupi
 */
public class VolcadorPDF {
    
    private static void abrirPDF() {
        Desktop desk = Desktop.getDesktop();
        try {
            desk.open(new File("tmpPdf.pdf"));
        } catch (IOException ex) {
            Logger.getLogger(VolcadorPDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void volcarPlanDeCuentas() throws FileNotFoundException, DocumentException {
        ArrayList<Cuenta> cuentas = new Cuenta().obtenerTodos("");
        
        Document doc = new Document();
        
        PdfWriter.getInstance(doc, new FileOutputStream("tmpPdf.pdf"));
        
        //Preparar la tabla
        PdfPTable table = new PdfPTable(3); //nueva tabla con dos columnas
        
        //Titulo de l tabla
        Font font = new Font(FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.WHITE);
        PdfPCell cell = new PdfPCell( new Phrase("Plan de Cuentas",font));
        cell.setColspan(3);
        cell.setBackgroundColor(BaseColor.GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        //Nombres de columnas
        cell = new PdfPCell( new Phrase("Código"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell( new Phrase("Cuenta"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell( new Phrase("¿Recibe Saldo?"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        //Todas las cuentas.
        font = new Font(FontFamily.HELVETICA, 11, Font.NORMAL, BaseColor.BLACK);
        for (Cuenta c : cuentas) {
            cell = new PdfPCell( new Phrase(c.getCodigo(),font));
            cell.setColspan(1);
            table.addCell(cell);
        
            cell = new PdfPCell( new Phrase(c.getNombre(),font));
            cell.setColspan(1);
            table.addCell(cell);
            
            if (c.isRecibeSaldo()) {
                cell = new PdfPCell( new Phrase("SÍ",font));
                cell.setColspan(1);
                table.addCell(cell);
            }
            else {
                cell = new PdfPCell( new Phrase("NO",font));
                cell.setColspan(1);
                table.addCell(cell);
            }
            
        }
        
        
        
        doc.open();
        doc.add(table);
        doc.close();
        
        abrirPDF();
        
    }
    
    
    
    public static void libroDiario(JTable tabla, Date fechaDesde, Date fechaHasta) throws DocumentException, FileNotFoundException {
        
        Document doc = new Document();
        
        PdfWriter.getInstance(doc, new FileOutputStream("tmpPdf.pdf"));
        
        //Preparar la tabla
        PdfPTable table = new PdfPTable(5); //nueva tabla con cinco columnas
        
        //Titulo de l tabla
        Font font = new Font(FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.WHITE);
        PdfPCell cell = new PdfPCell( new Phrase("Libro Diario",font));
        cell.setColspan(5);
        cell.setBackgroundColor(BaseColor.BLACK);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        //Fecha
        font = new Font(FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.WHITE);
        cell = new PdfPCell( new Phrase(FechaHora.fechaDMA(fechaDesde) + " - " + FechaHora.fechaDMA(fechaHasta),font));
        cell.setColspan(5);
        cell.setBackgroundColor(BaseColor.GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        //Nombres de columnas
        cell = new PdfPCell( new Phrase("Fecha"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell( new Phrase("Asiento"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell( new Phrase("Cuenta"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell( new Phrase("Debe"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell( new Phrase("Haber"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        int i;
        for (i=0;i<tabla.getRowCount();i++) {
            cell = new PdfPCell( new Phrase(tabla.getValueAt(i, 0).toString()));
            cell.setColspan(1);
            table.addCell(cell);
            cell = new PdfPCell( new Phrase(tabla.getValueAt(i, 1).toString()));
            cell.setColspan(1);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell( new Phrase(tabla.getValueAt(i, 2).toString()));
            cell.setColspan(1);
            table.addCell(cell);
            cell = new PdfPCell( new Phrase(tabla.getValueAt(i, 3).toString()));
            cell.setColspan(1);
            table.addCell(cell);
            cell = new PdfPCell( new Phrase(tabla.getValueAt(i, 4).toString()));
            cell.setColspan(1);
            table.addCell(cell);
        }     
        
            
        
        
        
        
        doc.open();
        doc.add(table);
        doc.close();
        
        abrirPDF();
        
    }
    
    public static void libroMayor(JTable tabla, double total, String cuenta) throws DocumentException, FileNotFoundException {
        
        Document doc = new Document();
        
        PdfWriter.getInstance(doc, new FileOutputStream("tmpPdf.pdf"));
        
        //Preparar la tabla
        PdfPTable table = new PdfPTable(5); //nueva tabla con cinco columnas
        
        //Titulo de l tabla
        Font font = new Font(FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.WHITE);
        PdfPCell cell = new PdfPCell( new Phrase("Libro Mayor",font));
        cell.setColspan(5);
        cell.setBackgroundColor(BaseColor.BLACK);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);       
        
         //Subtitulo
        font = new Font(FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.WHITE);
        cell = new PdfPCell( new Phrase(cuenta,font));
        cell.setColspan(5);
        cell.setBackgroundColor(BaseColor.GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);       
        
        
        //Nombres de columnas
        cell = new PdfPCell( new Phrase("Cuenta"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell( new Phrase("Asiento"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell( new Phrase("Debe"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell( new Phrase("Haber"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell( new Phrase("Saldo"));
        cell.setColspan(1);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        int i;
        for (i=0;i<tabla.getRowCount();i++) {
            cell = new PdfPCell( new Phrase(tabla.getValueAt(i, 0).toString()));
            cell.setColspan(1);
            table.addCell(cell);
            cell = new PdfPCell( new Phrase(tabla.getValueAt(i, 1).toString()));
            cell.setColspan(1);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell( new Phrase(tabla.getValueAt(i, 2).toString()));
            cell.setColspan(1);
            table.addCell(cell);
            cell = new PdfPCell( new Phrase(tabla.getValueAt(i, 3).toString()));
            cell.setColspan(1);
            table.addCell(cell);
            cell = new PdfPCell( new Phrase(tabla.getValueAt(i, 4).toString()));
            cell.setColspan(1);
            table.addCell(cell);
        }
        
        cell = new PdfPCell( new Phrase("Total: " + Double.toString(total)));
        cell.setColspan(5);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);
        
            
        
        
        
        
        doc.open();
        doc.add(table);
        doc.close();
        
        abrirPDF();
        
    }
    
    
    
    
    
}
