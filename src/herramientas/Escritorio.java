/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herramientas;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;

/**
 *
 * @author Lucas
 */
public class Escritorio extends JDesktopPane{
  
    Image img=new ImageIcon(getClass().getResource("/sistadm2/imagenes/Fondo2.jpg")).getImage();
    Image logo=new ImageIcon(getClass().getResource("/sistadm2/imagenes/logo.jpg")).getImage();
    
    @Override
    protected void paintComponent(Graphics g) {
       
        super.paintComponent(g);
        g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
        g.drawImage(logo, this.getWidth()/2-logo.getWidth(this)/2, this.getHeight()/2-logo.getHeight(this)/2, this);
        
    }
    
    
    }
    

