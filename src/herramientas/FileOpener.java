/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package herramientas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pupi
 */
public class FileOpener {
    File file;
    FileReader fr;
    BufferedReader br;
    
    public FileOpener (File file) throws FileNotFoundException {
        this.file = file;
        
        fr = new FileReader(file);
        
        br = new BufferedReader(fr);
    
        
    }
    
    public String abrir() throws IOException {
        StringBuffer buf = new StringBuffer("");
        
            String line = br.readLine();            
            while(line != null){
                buf.append(line);
                //buf.append("\n");
                line = br.readLine();                
            } 
        
        
            fr.close();
        
        return buf.toString();

    }
    
}
