/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herramientas;

import javax.swing.JOptionPane;

/**
 *
 * @author pupi
 */
public class Mensajes {
    
    public static int mensajeSiNo(String texto, String titulo) {
        return JOptionPane.showConfirmDialog(null, texto, titulo, JOptionPane.YES_NO_OPTION);
        
    }
    
    public static void mensaje(String texto) {
        JOptionPane.showMessageDialog(null, texto);
    }
    
    public static void mensaje(String texto, String titulo) {
        JOptionPane.showMessageDialog(null, texto,titulo, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void mensaje(String texto, String titulo, int icono){
        JOptionPane.showMessageDialog(null, texto, titulo, icono);
    }
    
    public static int mensajeSINO(String texto,String titulo) {
        return JOptionPane.showConfirmDialog(null, texto, titulo, JOptionPane.YES_NO_OPTION);
    }
    
    
}
