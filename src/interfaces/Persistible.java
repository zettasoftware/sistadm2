/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.sql.SQLException;

/**
 *
 * @author bibi
 */
public interface Persistible {
    public void insertar() throws SQLException;
    public void actualizar() throws SQLException;
    public void borrar() throws SQLException;
    
    

}