/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import herramientas.SQL;
import interfaces.Persistible;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pupi
 */
public class Mercaderia implements Persistible{
    
    private int cantidad;
    private int id;
    private double precioUnit;
    private double total;
    private double iva;
    private String codigo;
    private String detalle;
    

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }
    
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecioUnit() {
        return precioUnit;
    }

    public void setPrecioUnit(double precioUnit) {
        this.precioUnit = precioUnit;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void insertar() throws SQLException {        
        String consulta = "insert into mercaderia (cantidad,precioUnidad,total,iva,codigo,detalle) values ("
                + getCantidad() + "," + getPrecioUnit() + "," + getTotal() + "," + getIva() + ",'" + getCodigo() + "','" +
                getDetalle() + "')";
        
        System.out.println(consulta);
        
        
        SQL sql = SQL.getDbCon();
        
        sql.insert(consulta);
        
        setId(sql.getLastId());
    }
    
    public Mercaderia obtener() throws SQLException {
        SQL sql = SQL.getDbCon();
        
        String consulta = "select * from mercaderia";
        
        ResultSet rs = sql.query(consulta);
        rs.next();
        
        Mercaderia m = new Mercaderia();
        m.setId(rs.getInt("idMercaderia"));
        m.setCantidad(rs.getInt("cantidad"));
        m.setPrecioUnit(rs.getDouble("precioUnidad"));
        m.setTotal(rs.getDouble("total"));
        m.setIva(rs.getDouble("iva"));
        m.setCodigo(rs.getString("codigo"));
        m.setDetalle(rs.getString("detalle"));
        return m;
        
        
    }
    
    public ArrayList<Mercaderia> obtenerTodos(String filtro) throws SQLException {
        ArrayList<Mercaderia> aux = new ArrayList<>();
        String consulta;
        
        if (filtro.equals("")) {
            consulta = "select * from mercaderia order by codigo";
        }
        else {
            consulta = "select * from mercaderia where " + filtro + " order by codigo";
        }
        
        SQL sql = SQL.getDbCon();
        
        ResultSet rs = sql.query(consulta);
        
        while (rs.next()) {
            Mercaderia m = new Mercaderia();
            m.setId(rs.getInt("idMercaderia"));
            m.setCantidad(rs.getInt("cantidad"));
            m.setIva(rs.getDouble("iva"));
            m.setPrecioUnit(rs.getDouble("precioUnidad"));
            m.setTotal(rs.getDouble("total"));
            m.setCodigo(rs.getString("codigo"));
            m.setDetalle(rs.getString("detalle"));
            
            aux.add(m);
        }
        
        
        
        return aux;
    }

    @Override
    public void actualizar() throws SQLException {
    
        String consulta = "update mercaderia set cantidad=" + getCantidad() + 
                ",precioUnidad="+ getPrecioUnit()  +",total="+ getTotal() + 
                ",iva=" + getIva() + ",codigo='" + getCodigo() + "',detalle='" + getDetalle() + "'" 
                + " where idMercaderia=" + getId();
    
        SQL sql = SQL.getDbCon();
        
        sql.deleteUpdate(consulta);
    
    }

    @Override
    public void borrar() throws SQLException {
        String consulta = "update mercaderia set borrado=1 where idMercaderia=" + getId();
        
        SQL sql = SQL.getDbCon();
        
        sql.deleteUpdate(consulta);
        
    }
    
    public void recuperar() throws SQLException {
        String consulta = "update mercaderia set borrado=0 where idMercaderia=" + getId();
        
        SQL sql = SQL.getDbCon();
        
        sql.deleteUpdate(consulta);
    }

    @Override
    public String toString() {
        return detalle;
    }
    
    
    
       
    
    
    
    
    
    
    
}
