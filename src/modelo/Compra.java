/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import herramientas.FechaHora;
import herramientas.SQL;
import interfaces.Persistible;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author pupi
 */
public class Compra implements Persistible {
    private int id;
    private Mercaderia producto;
    private int cantidad;
    private double monto;
    private Proveedor proveedor;
    private boolean aCredito;
    private boolean borrado;
    private Date fecha;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }
    

    public Mercaderia getProducto() {
        return producto;
    }

    public void setProducto(Mercaderia producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public boolean isaCredito() {
        return aCredito;
    }

    public void setaCredito(boolean aCredito) {
        this.aCredito = aCredito;
    }

    public boolean isBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }
    
    @Override
    public void insertar() throws SQLException {
        String consulta = "insert into compras (cantidad,monto,idProveedor,idMercaderia,aCredito,fecha) values ("
                + getCantidad() + "," + getMonto() + "," + getProveedor().getId()+ "," + getProducto().getId() + "," + isaCredito() + ",'" + FechaHora.fechaAMD(getFecha()) + "')";
        
        System.out.println(consulta);        
        
        SQL sql = SQL.getDbCon();
        
        sql.insert(consulta);
        
        setId(sql.getLastId());
    }

    @Override
    public void actualizar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void borrar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public double calcularMontoTotal() {
        return this.getMonto() + this.getMonto() * producto.getIva() / 100;
    }
    
    public ArrayList<Compra> obtenerTodos(String filtro) throws SQLException {
        ArrayList<Compra> aux = new ArrayList<>();
        String consulta;
        
        if (filtro.equals("")) {
            consulta = "select * from compras order by fecha";
        }
        else {
            consulta = "select * from compras where " + filtro + " order by fecha";
        }
        
        SQL sql = SQL.getDbCon();
        
        ResultSet rs = sql.query(consulta);
        
        while (rs.next()) {
            Compra c = new Compra();
            c.setCantidad(rs.getInt("cantidad"));
            c.setMonto(rs.getDouble("monto"));
            c.setId(rs.getInt("idCompras"));
            Mercaderia m = new Mercaderia().obtenerTodos("idMercaderia=" + Integer.toString(rs.getInt("idMercaderia"))).get(0);
            c.setProducto(m);
            Proveedor p = new Proveedor().obtenerTodos("idProveedor=" + Integer.toString(rs.getInt("idProveedor"))).get(0);
            c.setProveedor(p);
            c.setFecha(rs.getDate("fecha"));
            
            aux.add(c);
        }
        
        return aux;
    }
    
}
