/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import herramientas.SQL;
import herramientas.Validador;
import interfaces.Persistible;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pupi
 */
public class Cuenta implements Persistible{
    
    private int id;
    private String codigo;
    private String nombre;
    private boolean sumaDebe;
    private boolean recibeSaldo;
        

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isSumaDebe() {
        return sumaDebe;
    }

    public void setSumaDebe(boolean sumaDebe) {
        this.sumaDebe = sumaDebe;
    }

    public boolean isRecibeSaldo() {
        return recibeSaldo;
    }

    public void setRecibeSaldo(boolean recibeSaldo) {
        this.recibeSaldo = recibeSaldo;
    }

    @Override
    public void insertar() throws SQLException {
        String consulta = "insert into cuentas (codigo,nombre,sumaDebe,recibe_saldo) values "
                + "('" + getCodigo() + "','" + getNombre() + "'," 
                + Validador.booleanAInt(isSumaDebe()) + "," 
                + Validador.booleanAInt(isRecibeSaldo()) + ")";
        
        SQL sql = SQL.getDbCon();
        
        sql.insert(consulta);
        
        setId(sql.getLastId());
        
    }

    @Override
    public void actualizar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void borrar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int obtenerNumeroHijos() throws SQLException {
        SQL sql = SQL.getDbCon();
        int aux=0;
        
        String consulta = "select count(*) from cuentas where codigo like '" + getCodigo() + "._'";
        
        ResultSet rs = sql.query(consulta);
        
        rs.next();
        
        aux = rs.getInt(1);
        
        return aux;
    }
    
    public Cuenta obtenerCuentaPorCodigo(String codigo) throws SQLException {
        SQL sql = SQL.getDbCon();
        String consulta = "select * from cuentas where codigo='"+codigo+"'"; 
        ResultSet rs = sql.query(consulta);
        
        rs.next();
                Cuenta c = new Cuenta();
                c.setId(rs.getInt("idCuentas"));
                c.setCodigo(rs.getString("codigo"));
                c.setNombre(rs.getString("nombre"));
                c.setSumaDebe(rs.getBoolean("sumaDebe"));
                c.setRecibeSaldo(rs.getBoolean("recibe_saldo"));
                
        return c;
            
    }
    
    public ArrayList<Cuenta> obtenerTodos(String filtro) {
        SQL sql = SQL.getDbCon();
        ArrayList<Cuenta> aux = new ArrayList<>();
        String consulta = "";
        
        if (filtro.equals("")) {
            consulta = "select * from cuentas order by codigo";
        }
        else {
            consulta = "select * from cuentas where " + filtro + " order by codigo";
        }
        
        
        try {
            ResultSet rs = sql.query(consulta);
            
            while (rs.next()) {
                Cuenta c = new Cuenta();
                c.setId(rs.getInt("idCuentas"));
                c.setCodigo(rs.getString("codigo"));
                c.setNombre(rs.getString("nombre"));
                c.setSumaDebe(rs.getBoolean("sumaDebe"));
                c.setRecibeSaldo(rs.getBoolean("recibe_saldo"));
                
                aux.add(c);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Cuenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
            
        
        
        return aux;
        
        
    }

    @Override
    public String toString() {
        return this.getNombre();
    }
    
    
    
    
}

