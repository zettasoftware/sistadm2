/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import herramientas.FechaHora;
import herramientas.SQL;
import interfaces.Persistible;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author pupi
 */
public class Venta implements Persistible{
    
    private int id;
    private Mercaderia producto;
    private int cantidad;
    private double monto;
    private boolean aCredito;
    private boolean borrado;
    private Date fecha;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Mercaderia getProducto() {
        return producto;
    }

    public void setProducto(Mercaderia producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public boolean isaCredito() {
        return aCredito;
    }

    public void setaCredito(boolean aCredito) {
        this.aCredito = aCredito;
    }

    public boolean isBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public void insertar() throws SQLException {
        String consulta = "insert into ventas (cantidad,monto,idMercaderia,aCredito,fecha) values ("
                + getCantidad() + "," + getMonto() + "," + getProducto().getId() + "," + isaCredito() + ",'" + FechaHora.fechaAMD(getFecha()) + "')";
        
        System.out.println(consulta);        
        
        SQL sql = SQL.getDbCon();
        
        sql.insert(consulta);
        
        setId(sql.getLastId());
    }

    @Override
    public void actualizar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void borrar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public double calcularMontoTotal() {
        return this.getMonto() + this.getMonto() * producto.getIva() / 100;
    }
    
    public ArrayList<Venta> obtenerTodos(String filtro) throws SQLException {
        ArrayList<Venta> aux = new ArrayList<>();
        String consulta;
        
        if (filtro.equals("")) {
            consulta = "select * from ventas order by fecha";
        }
        else {
            consulta = "select * from ventas where " + filtro + " order by fecha";
        }
        
        SQL sql = SQL.getDbCon();
        
        ResultSet rs = sql.query(consulta);
        
        while (rs.next()) {
            Venta v = new Venta();
            v.setCantidad(rs.getInt("cantidad"));
            v.setMonto(rs.getDouble("monto"));
            v.setId(rs.getInt("idventas"));
            Mercaderia m = new Mercaderia().obtenerTodos("idMercaderia=" + Integer.toString(rs.getInt("idMercaderia"))).get(0);
            v.setProducto(m);
            v.setFecha(rs.getDate("fecha"));
            
            aux.add(v);
        }
        
        return aux;
    }
    
}
