/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import herramientas.FechaHora;
import herramientas.SQL;
import interfaces.Persistible;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;



/**
 *
 * @author pupi
 */
public class Asiento implements Persistible{
    
    private int id;
    private Date fecha;
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    
  

    @Override
    public void insertar() throws SQLException {
        String consulta = "insert into asientos (fecha) values ('"
                + FechaHora.fechaAMD(getFecha()) + "')" ;
        SQL sql = SQL.getDbCon();
        
        sql.insert(consulta);
        
        setId(sql.getLastId());
        
        
        
    }

    @Override
    public void actualizar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void borrar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
