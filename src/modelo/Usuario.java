/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import excepciones.InvalidLoginException;
import herramientas.SQL;
import interfaces.Persistible;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pupi
 */
public class Usuario implements Persistible {

    @Override
    public void insertar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void borrar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }   
    
    public Usuario loguearse() throws InvalidLoginException {
        String filtro = "nombreUsuario='" + getNombreUsuario() + "' and "
                + "password='" + getPassword() + "' and " + "borrado=0" ;
        
        ArrayList<Usuario> ret = new ArrayList<>();
        
        try {
            ret = obtenerTodos(filtro);
        } catch (SQLException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (ret.isEmpty()) {
            throw new InvalidLoginException();
        }
        else {
            return ret.get(0);
        }
        
        
    }
    
    public ArrayList<Usuario> obtenerTodos(String filtro) throws SQLException {
        ArrayList<Usuario> aux = new ArrayList<>();
        String consulta;
        
        if (filtro.equals("")) {
            consulta = "select * from usuarios";
        }         
        else {
            consulta = "select * from usuarios where " + filtro;
        }
        
        SQL sql = SQL.getDbCon();
        
        ResultSet rs = sql.query(consulta);
        
        while (rs.next()) {
            Usuario u = new Usuario();
            u.setId(rs.getInt("idUsuario"));
            u.setNombreApellido(rs.getString("nombreApellido"));
            u.setNombreUsuario(rs.getString("nombreUsuario"));
            u.setPassword(rs.getString("password"));
            u.setPermisos(rs.getInt("permisos"));
            
            aux.add(u);
        }        
        
        return aux;
    }
    
    
    private int id;
    private String nombreApellido;
    private String nombreUsuario;
    private String password;
    private int permisos; //Admin 0, Vendedores 1

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNombreApellido() {
        return nombreApellido;
    }

    public void setNombreApellido(String nombreApellido) {
        this.nombreApellido = nombreApellido;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPermisos() {
        return permisos;
    }

    public void setPermisos(int permisos) {
        this.permisos = permisos;
    }

    public Usuario(String nombreApellido, String nombreUsuario, String password, int permisos) {
        this.nombreApellido = nombreApellido;
        this.nombreUsuario = nombreUsuario;
        this.password = password;
        this.permisos = permisos;
    }

    public Usuario() {
    }
     
    
    
}
