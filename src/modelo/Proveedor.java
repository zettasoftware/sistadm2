/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import herramientas.SQL;
import interfaces.Persistible;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pupi
 */
public class Proveedor implements Persistible{
    
    private String nombre;
    private String cuit;
    private String direccion;
    private String telefono;
    private String mail;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
  
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    
    public Proveedor obtener(Long id) throws SQLException{
        SQL sql = SQL.getDbCon();
        
        String consulta = "select * from Proveedor were idProveedor = " + id + " and borrado=0";
        
        ResultSet rs = sql.query(consulta);
        rs.next();
        
        Proveedor p = new Proveedor();
        p.setCuit(rs.getString("cuit"));
        p.setDireccion(rs.getString("direccion"));
        p.setMail(rs.getString("mail"));
        p.setTelefono(rs.getString("telefono"));
        
        return p;
    }

    @Override
    public void insertar() throws SQLException {
        String consulta = "insert into proveedor (cuit,nombre,direccion,telefono,mail) values ('"
                + getCuit() + "','" + getNombre() + "','" + getDireccion() + "','" + getTelefono() + "','" + getMail() + "')";
        
        System.out.println(consulta);
        
        
        SQL sql = SQL.getDbCon();
        
        sql.insert(consulta);
        
        setId(sql.getLastId());
    }

    @Override
    public void actualizar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void borrar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ArrayList<Proveedor> obtenerTodos(String filtro) throws SQLException {
        ArrayList<Proveedor> aux = new ArrayList<>();
        String consulta;
        
        if (filtro.equals("")) {
            consulta = "select * from proveedor order by cuit";
        }
        else {
            consulta = "select * from proveedor where " + filtro + " order by cuit";
        }
        
        SQL sql = SQL.getDbCon();
        
        ResultSet rs = sql.query(consulta);
        
        while (rs.next()) {
            Proveedor p = new Proveedor();
            p.setCuit(rs.getString("cuit"));
            p.setDireccion(rs.getString("direccion"));
            p.setId(rs.getInt("idProveedor"));
            p.setMail(rs.getString("mail"));
            p.setNombre(rs.getString("nombre"));
            p.setTelefono(rs.getString("telefono"));
            
            aux.add(p);
        }
        
        return aux;
    }

    @Override
    public String toString() {
        return this.getNombre() + " || " + this.getCuit();
    }
    
    
   
}
