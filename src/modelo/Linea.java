/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import herramientas.SQL;
import interfaces.Persistible;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pupi
 */

public class Linea implements Persistible{
    
    private int id;
    private String descripcion;
    private double monto;
    private Cuenta cuenta;
    private Asiento asiento;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Asiento getAsiento() {
        return asiento;
    }

    public void setAsiento(Asiento asiento) {
        this.asiento = asiento;
    }
    
    
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    @Override
    public void insertar() throws SQLException {
        String consulta = " insert into lineas (idAsiento,idLineas,descripcion,monto,cuenta)"
                + " values (" + getAsiento().getId() + "," + getId() + ",'"
                + getDescripcion() + "'," + getMonto() + "," + getCuenta().getId() + ")";
        
        SQL sql = SQL.getDbCon();
        
        sql.insert(consulta);
        
    }

    @Override
    public void actualizar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void borrar() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ArrayList<Linea> obtenerTodos(String filtro) throws SQLException {
        ArrayList<Linea> aux = new ArrayList<>();
        
        String consulta = "select a.idAsiento,a.fecha,l.idLineas,l.descripcion,l.monto,l.cuenta," +
                "c.idCuentas,c.codigo,c.nombre,c.sumaDebe,c.recibe_saldo "
                + " from asientos a inner join lineas l on a.idAsiento=l.idAsiento "
                + "inner join cuentas c on l.cuenta = c.idCuentas where " + filtro + 
                " order by a.fecha, a.idAsiento, l.idLineas";
    
        //System.out.println(consulta);
        
        SQL sql = SQL.getDbCon();
        
        ResultSet rs = sql.query(consulta);
        
        
        
        
        while (rs.next()) {
            Linea l = new Linea();
            Cuenta c = new Cuenta();
            
            c.setId(rs.getInt("idCuentas"));
            c.setCodigo(rs.getString("nombre"));
            c.setNombre(rs.getString("nombre"));
            c.setRecibeSaldo(rs.getBoolean("recibe_saldo"));
            c.setSumaDebe(rs.getBoolean("sumaDebe"));
            l.setCuenta(c);
            
            Asiento a = new Asiento();
            a.setFecha(rs.getDate("fecha"));
            a.setId(rs.getInt("idAsiento"));
            l.setAsiento(a);
            
            l.setDescripcion(rs.getString("descripcion"));
            l.setId(rs.getInt("idLineas"));
            l.setMonto(rs.getDouble("monto"));
            
            aux.add(l);
            
        }
        
        return aux;
        
        
    
    }
    
    
}
