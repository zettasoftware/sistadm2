-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: sistadm2
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asientos`
--

DROP TABLE IF EXISTS `asientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asientos` (
  `idAsiento` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  PRIMARY KEY (`idAsiento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asientos`
--

LOCK TABLES `asientos` WRITE;
/*!40000 ALTER TABLE `asientos` DISABLE KEYS */;
/*!40000 ALTER TABLE `asientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuentas`
--

DROP TABLE IF EXISTS `cuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentas` (
  `idCuentas` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `sumaDebe` tinyint(1) NOT NULL,
  `recibe_saldo` tinyint(1) NOT NULL,
  PRIMARY KEY (`idCuentas`),
  UNIQUE KEY `plan_cuentas_idx` (`codigo`),
  UNIQUE KEY `plan_cuentas_idx1` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuentas`
--

LOCK TABLES `cuentas` WRITE;
/*!40000 ALTER TABLE `cuentas` DISABLE KEYS */;
INSERT INTO `cuentas` VALUES (1,'1','Activo',0,0),(2,'1.1','Caja y Bancos',0,0),(3,'1.1.1','Caja',1,1),(4,'1.1.2','Banco Plazo Fijo',1,1),(5,'1.1.3','Banco c/c',1,1),(6,'1.2','Creditos',0,0),(7,'1.2.1','Deudores por Ventas',1,1),(8,'1.2.2','Documento a Cobrar',1,1),(9,'1.2.3','Valores a Depositar',1,1),(10,'1.3','Bienes de Cambio',0,0),(11,'1.3.1','Mercaderias',1,1),(12,'1.4','Bienes de Uso',0,0),(13,'1.4.1','Inmuebles',1,1),(14,'1.4.2','Rodados',1,1),(15,'1.4.3','Instalaciones',1,1),(16,'2','Pasivo',0,0),(57,'2.1','Deudas Comerciales',0,0),(58,'2.1.1','Proveedores',0,1),(59,'2.1.2','Sueldos a Pagar',0,1),(60,'2.2','Deudas Fiscales',0,0),(61,'2.2.1','Impuestos a Pagar',0,1),(62,'2.2.2','Moratorias',0,1),(63,'2.3','Prestamos Bancarios',0,0),(64,'3','Patrimonio',0,0),(65,'3.1','Capital',0,1),(66,'3.2','Resultados',0,1),(67,'4','Ingresos',0,0),(68,'4.1','Venta',0,0),(69,'4.1.1','Ventas',0,1),(70,'4.2','Otros Ingresos',0,1),(71,'4.3','Intereses Ganados',0,1),(72,'5','Egresos',0,0),(73,'5.1','CMV',1,1),(74,'5.2','Sueldos',1,1),(75,'5.3','Intereses',1,1),(76,'5.4','Alquileres',1,1),(77,'2.1.3','Acreedores',0,1),(83,'6','IVA',0,0),(84,'6.1','Credito Fiscal',1,1),(85,'6.2','Debito Fiscal',0,1),(86,'1.1.3.1','Banco Rio c/c',1,1),(87,'1.1.2.1','Banco Nacion Plazo Fijo',1,1),(88,'1.1.2.2','Banco Provincia P/Fijo',1,1);
/*!40000 ALTER TABLE `cuentas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `idEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `cuit` varchar(20) NOT NULL,
  `razonsocial` varchar(30) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  PRIMARY KEY (`idEmpresa`),
  UNIQUE KEY `empresa_idx` (`cuit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lineas`
--

DROP TABLE IF EXISTS `lineas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lineas` (
  `idAsiento` int(11) NOT NULL,
  `idLineas` int(11) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  `monto` decimal(30,2) NOT NULL,
  `cuenta` int(11) NOT NULL,
  PRIMARY KEY (`idAsiento`,`idLineas`),
  KEY `plan_cuentas_lineas_fk` (`cuenta`),
  CONSTRAINT `asiento_lineas_fk` FOREIGN KEY (`idAsiento`) REFERENCES `asientos` (`idAsiento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `plan_cuentas_lineas_fk` FOREIGN KEY (`cuenta`) REFERENCES `cuentas` (`idCuentas`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lineas`
--

LOCK TABLES `lineas` WRITE;
/*!40000 ALTER TABLE `lineas` DISABLE KEYS */;
/*!40000 ALTER TABLE `lineas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mercaderia`
--

DROP TABLE IF EXISTS `mercaderia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mercaderia` (
  `idMercaderia` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `precioUnidad` decimal(30,2) NOT NULL,
  `total` decimal(30,2) NOT NULL,
  `iva` decimal(10,2) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `detalle` varchar(100) NOT NULL,
  `borrado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idMercaderia`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mercaderia`
--

LOCK TABLES `mercaderia` WRITE;
/*!40000 ALTER TABLE `mercaderia` DISABLE KEYS */;
INSERT INTO `mercaderia` VALUES (1,0,0.00,0.00,21.00,'123','coca',0),(2,0,0.00,0.00,21.00,'1234','sprite',0),(5,0,0.00,0.00,21.00,'12345','coca',0);
/*!40000 ALTER TABLE `mercaderia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `idProveedor` int(11) NOT NULL AUTO_INCREMENT,
  `cuit` varchar(60) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `direccion` varchar(150) NOT NULL,
  `telefono` varchar(150) NOT NULL,
  `mail` varchar(150) NOT NULL,
  `borrado` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`idProveedor`),
  UNIQUE KEY `cuit_UNIQUE` (`cuit`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1,'123','Marin','312313','31233','313','\0');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombreApellido` varchar(45) NOT NULL,
  `nombreUsuario` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `permisos` int(11) NOT NULL,
  `borrado` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `usuarios_idx` (`nombreUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Fernando','admin','123',0,0),(2,'pupi','minion','banana',1,0);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-07 21:06:39
